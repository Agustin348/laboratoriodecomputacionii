package DesafioIndividual6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class PanelConBotones extends JPanel {

    JButton botonRojo = new JButton("ROJO");
    JButton botonAZul = new JButton("AZUL");

    public PanelConBotones(){
        add(botonRojo);
        botonRojo.addActionListener(new ColorDePanel(Color.RED));
        add(botonAZul);
        botonAZul.addActionListener(new ColorDePanel(Color.BLUE));
        addMouseListener(new EventoDeRaton());
        EventoDeFoco eventoDeFoco = new EventoDeFoco();
        botonAZul.addFocusListener(eventoDeFoco);
        botonRojo.addFocusListener(eventoDeFoco);
    }

    private class ColorDePanel implements ActionListener{

        private Color colorDeFondo;

        public ColorDePanel(Color colorDeFondo) {
            this.colorDeFondo = colorDeFondo;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            setBackground(colorDeFondo);

        }
    }

    private class EventoDeFoco implements FocusListener {


        @Override
        public void focusGained(FocusEvent e) {
            System.out.println("El elemento ha ganado el foco");
        }

        @Override
        public void focusLost(FocusEvent e) {
            System.out.println("El elemento ha Perdido el foco");
        }
    }

    private class EventoDeRaton extends MouseAdapter {
        @Override
        public void mouseClicked(MouseEvent e) {
            System.out.println("Se ha producido algun evento");
        }

    }
}