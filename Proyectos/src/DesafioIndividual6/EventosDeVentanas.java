package DesafioIndividual6;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class EventosDeVentanas extends WindowAdapter {
    @Override
    public void windowOpened(WindowEvent e) {
        System.out.println("Abierto");
    }

    @Override
    public void windowClosing(WindowEvent e) {
        System.out.println("Cerradose");
    }

    @Override
    public void windowClosed(WindowEvent e) {
        System.out.println("Cerrado");
    }
}