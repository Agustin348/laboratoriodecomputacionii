package DesafioIndividual6;

import javax.swing.*;
import java.awt.*;

public class MyFrame extends JFrame {

    public MyFrame(){

        setTitle("Proyecto");

        Toolkit mipantalla = Toolkit.getDefaultToolkit();

        Dimension tamanoPantalla = mipantalla.getScreenSize();

        int alturaPantalla = tamanoPantalla.height;
        int anchuraPantalla = tamanoPantalla.width;

        setSize(anchuraPantalla/3, alturaPantalla/3);
        setLocation(anchuraPantalla/3, alturaPantalla/3);

        setVisible(true);

        PanelConBotones panel = new PanelConBotones();
        add(panel);

    }
}
