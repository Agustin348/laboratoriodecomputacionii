package TrabajoIndividualBanco;

class Operaciones {
	
	public static void Tranferencias(CuentaCorriente cuentaIngreso, CuentaCorriente cuentaEgreso, double monto) {
		
		cuentaIngreso.IngresarDinero(monto);
		cuentaEgreso.ExtraerDinero(monto);
	}

}
