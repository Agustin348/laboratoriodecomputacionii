package TrabajoIndividualBanco;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

class Banco {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		double saldo;
		int numCuenta;
		
		CuentaCorriente cta1 = new CuentaCorriente("Agustin", 1000, 23456);
		CuentaCorriente cta2 = new CuentaCorriente("Paula", 2000, 34567);
		
		System.out.println("------Banco------");
		System.out.println(" ");
		do {
			System.out.println("Ingrese operacion que desea realizar:");
			System.out.println(" ");
			System.out.println("1.Ingresar dinero en una cuenta");
			System.out.println("2.Sacar dinero de una cuenta");
			System.out.println("3.Mostrar el saldo de una cuenta");
			System.out.println("4.Motrar los datos de una cuenta");
			System.out.println("5.Hacer transferencias de dinero entre dos cuentas");
			System.out.println("6.Listar las cuentas corrientes cargadas");
			System.out.println("7.Almacenar en un archivo las cuentas corrientes cargadas.");
			
			
			switch(sc.nextInt()) {
				case 1: {
					
					System.out.println("Ingrese numero de cuenta");
					numCuenta = sc.nextInt();
					
					while(numCuenta != cta1.getNumCuenta() && numCuenta != cta2.getNumCuenta()) {
						System.out.println("La cuenta no existe, vuelva a dijitar");
						System.out.println("Ingrese numero de cuenta");
						numCuenta = sc.nextInt();
					}
					
					
					System.out.println("Ingrese cantidad de dinero");
					saldo = sc.nextDouble();
					
					if(numCuenta == cta1.getNumCuenta()) {
						cta1.IngresarDinero(saldo);
					}
					if(numCuenta == cta2.getNumCuenta()) {
						cta2.IngresarDinero(saldo);
					}
					
					break;
				}
				case 2: {
					
					System.out.println("Ingrese numero de cuenta");
					numCuenta = sc.nextInt();
					
					while(numCuenta != cta1.getNumCuenta() && numCuenta != cta2.getNumCuenta()) {
						System.out.println("La cuenta no existe, vuelva a dijitar");
						System.out.println("Ingrese numero de cuenta");
						numCuenta = sc.nextInt();
					}
					
					
					System.out.println("Ingrese cantidad de dinero");
					saldo = sc.nextDouble();
					
					if(numCuenta == cta1.getNumCuenta()) {
						cta1.ExtraerDinero(saldo);
					}
					if(numCuenta == cta2.getNumCuenta()) {
						cta2.ExtraerDinero(saldo);
					}
					break;
				}
				
				case 3: {
					System.out.println("Ingrese numero de cuenta");
					numCuenta = sc.nextInt();
					
					while(numCuenta != cta1.getNumCuenta() && numCuenta != cta2.getNumCuenta()) {
						System.out.println("La cuenta no existe, vuelva a dijitar");
						System.out.println("Ingrese numero de cuenta");
						numCuenta = sc.nextInt();
					}
					
					if(numCuenta == cta1.getNumCuenta()) {
						System.out.println(cta1.getSaldo());
					}
					if(numCuenta == cta2.getNumCuenta()) {
						System.out.println(cta2.getSaldo());
					}
					break;
				}
				
				case 4: {
					System.out.println("Ingrese numero de cuenta");
					numCuenta = sc.nextInt();
					
					while(numCuenta != cta1.getNumCuenta() && numCuenta != cta2.getNumCuenta()) {
						System.out.println("La cuenta no existe, vuelva a dijitar");
						System.out.println("Ingrese numero de cuenta");
						numCuenta = sc.nextInt();
					}
					
					if(numCuenta == cta1.getNumCuenta()) {
						System.out.println(cta1.toString());
					}
					if(numCuenta == cta2.getNumCuenta()) {
						System.out.println(cta2.toString());
					}
					break;
				}
				case 5: {
					System.out.println("Ingrese numero de cuenta de ingreso");
					numCuenta = sc.nextInt();
					
					while(numCuenta != cta1.getNumCuenta() && numCuenta != cta2.getNumCuenta()) {
						System.out.println("La cuenta no existe, vuelva a dijitar");
						System.out.println("Ingrese numero de cuenta");
						numCuenta = sc.nextInt();
					}
					
					System.out.println("Ingrese numero de cuenta de ingreso");
					
					if(numCuenta == cta1.getNumCuenta()) {
						Operaciones.Tranferencias(cta1, cta2, sc.nextDouble());
					}
					if(numCuenta == cta2.getNumCuenta()) {
						Operaciones.Tranferencias(cta2, cta1, sc.nextDouble());
					}
					
					System.out.println(cta1.toString());
					System.out.println(cta2.toString());
					break;
				}
				
				case 6: {
					
					Set<CuentaCorriente> listaDeCuentas = new HashSet<CuentaCorriente>();
					listaDeCuentas.add(cta1);
					listaDeCuentas.add(cta2);
					
					System.out.println(listaDeCuentas.toString());
					break;
				}
				
				case 7: {
					Set<CuentaCorriente> listaDeCuentas = new HashSet<CuentaCorriente>();
					listaDeCuentas.add(cta1);
					listaDeCuentas.add(cta2);
					
					try {
						ObjectOutputStream flujoDeSalida = new ObjectOutputStream(new FileOutputStream("C:\\Users\\aguss\\Desktop\\CARRERAS\\TSP\\SEGUNDO CUATRIMESTRE\\LABORATORIO DE COMPUTACION 2\\TrabajoIndividualBanco\\src\\TrabajoIndividualBanco\\listaDeCuentas.dat"));
						flujoDeSalida.writeObject(listaDeCuentas);
						flujoDeSalida.close();
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					
					try {
						ObjectInputStream flujoDeEntrada = new ObjectInputStream(new FileInputStream("C:\\Users\\aguss\\Desktop\\CARRERAS\\TSP\\SEGUNDO CUATRIMESTRE\\LABORATORIO DE COMPUTACION 2\\TrabajoIndividualBanco\\src\\TrabajoIndividualBanco\\listaDeCuentas.dat"));
						
						Set<CuentaCorriente> listaDeCuentasEntrada = (Set<CuentaCorriente>) flujoDeEntrada.readObject();
						
						System.out.println("Entrada: "+ listaDeCuentasEntrada.toString());
						
						flujoDeEntrada.close();
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} 
					break;
				}
			}
			
			System.out.println("Desea salir? s/n");
			
		}while(sc.next() != "s");
		
	}

}
