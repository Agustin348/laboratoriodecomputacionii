package TrabajoIndividualBanco;

import java.io.Serializable;

class CuentaCorriente implements Serializable {
	
	private String nombre;
	private double saldo;
	private int numCuenta;
	
	
	public CuentaCorriente(String nombre, double saldo, int numCuenta) {
		this.nombre = nombre;
		this.saldo = saldo;
		this.numCuenta = numCuenta;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public double getSaldo() {
		return saldo;
	}


	public void IngresarDinero(double dinero) {
		this.saldo = this.saldo + dinero;
	}
	
	public void ExtraerDinero(double dinero) {
		this.saldo = this.saldo - dinero;
	}

	public int getNumCuenta() {
		return numCuenta;
	}


	public void setNumCuenta(int numCuenta) {
		this.numCuenta = numCuenta;
	}


	@Override
	public String toString() {
		return "CuentaCorriente [nombre=" + nombre + ", saldo=" + saldo + ", numCuenta=" + numCuenta + "]";
	}
	
}
