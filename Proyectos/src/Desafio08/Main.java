package Desafio08;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner sc = new Scanner(System.in);
		
		int num;
		double resultado;
		
		System.out.print("Ingrese una numero: ");
		num = sc.nextInt();
		
		RaizScanner op = new RaizScanner();
		resultado = op.raizCuadrada(num);
		
		System.out.print("La raiz cuadrada de "+num+" es: "+resultado);
		
		sc.close();

	}

}
