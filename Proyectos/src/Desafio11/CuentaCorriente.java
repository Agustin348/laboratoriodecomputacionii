package Desafio11;

import java.util.Random;

public class CuentaCorriente {

    private double saldo;
    private String nombreTitular;
    private long numeroCuenta;

    public CuentaCorriente(String nombreTitular, double saldo){
        this.nombreTitular = nombreTitular;
        this.saldo = saldo;

        Random aleatorio = new Random();
        this.numeroCuenta = Math.abs(aleatorio.nextLong());
    }

    public String setSaldo(double monto) {

        if(monto > 0){
            this.saldo += monto;
            return "Se ingreso $"+monto;
        }
        else {
            return "El monto ingresado debe ser mayor a cero";
        }
    }
    public void setExtracion(double monto){
        this.saldo -= monto;
    }

    public static void Transferencia(CuentaCorriente ctaSalida, CuentaCorriente ctaEntrada, double monto){
        ctaEntrada.saldo += monto;
        ctaSalida.saldo -= monto;
    }

    @Override
    public String toString() {
        return "\nNombre Titular: " + nombreTitular +
                "\nSaldo: " + saldo + "\nNumeroCuenta: " + numeroCuenta;
    }
}
