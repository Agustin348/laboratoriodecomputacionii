package TrabajoPracticoLaCalculadora;

import javax.swing.*;
import java.awt.*;

public class Frame extends JFrame {

    public Frame(){
        setTitle("Calculadora");
        Toolkit mipantalla = Toolkit.getDefaultToolkit();

        Dimension tamanoPantalla = mipantalla.getScreenSize();

        int alturaPantalla = tamanoPantalla.height;
        int anchuraPantalla = tamanoPantalla.width;

        setSize(anchuraPantalla/4, alturaPantalla/2);
        setLocation(anchuraPantalla/3, alturaPantalla/4);

        PanelCalculadora panel = new PanelCalculadora();
        add(panel);

        setVisible(true);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    }
}
