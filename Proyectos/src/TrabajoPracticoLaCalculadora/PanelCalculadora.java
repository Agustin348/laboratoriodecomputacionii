package TrabajoPracticoLaCalculadora;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PanelCalculadora extends JPanel {

    private JButton pantalla;
    private JPanel LaminaNumeros;
    private boolean comienzo;
    private double resultado;
    private String ultimoValor;

    public PanelCalculadora(){
        setLayout(new BorderLayout());
        pantalla = new JButton("0");
        pantalla.setEnabled(false);
        add(pantalla, BorderLayout.NORTH);

        LaminaNumeros = new JPanel();
        LaminaNumeros.setLayout(new GridLayout(5,4));
        comienzo = true;

        ActionListener oyente = new NumeroEnPantalla();
        ActionListener operacion = new OperacionesDeCalculadora();

        NuevoBoton("", null);
        NuevoBoton("", null);
        NuevoBoton("", null);
        NuevoBoton("c", operacion);

        NuevoBoton("7", oyente);
        NuevoBoton("8", oyente);
        NuevoBoton("9", oyente);
        NuevoBoton("x", operacion);

        NuevoBoton("4", oyente);
        NuevoBoton("5", oyente);
        NuevoBoton("6", oyente);
        NuevoBoton("-", operacion);

        NuevoBoton("1", oyente);
        NuevoBoton("2", oyente);
        NuevoBoton("3", oyente);
        NuevoBoton("+", operacion);

        NuevoBoton("0", oyente);
        NuevoBoton(".", oyente);
        NuevoBoton("=", operacion);
        NuevoBoton("/", operacion);

        add(LaminaNumeros, BorderLayout.CENTER);
        ultimoValor = "=";

    }
    private void NuevoBoton(String txtBoton, ActionListener oyente){
        JButton boton = new JButton(txtBoton);
        boton.addActionListener(oyente);
        LaminaNumeros.add(boton);
    }
    private class NumeroEnPantalla implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String entrada = e.getActionCommand();

            if(comienzo){
                pantalla.setText("");
                comienzo = false;
            }

            pantalla.setText(pantalla.getText() + entrada);
        }
    }
    private class OperacionesDeCalculadora implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {

            String operacion = e.getActionCommand();

            if(operacion.equals("c")){

                pantalla.setText("0.0");
                resultado = 0;
            }
            else {

                Calculo(Double.parseDouble(pantalla.getText()));
                ultimoValor = operacion;
                comienzo = true;
            }
        }
        public void Calculo(Double valor){

            boolean hasError = false;

            if(ultimoValor.equals("+")){
                resultado += valor;
            }
            if(ultimoValor.equals("-")){
                resultado -= valor;
            }
            if(ultimoValor.equals("/")) {
                resultado /= valor;
                if (Double.isInfinite(resultado)){
                    hasError = true;
                }
            }
            if(ultimoValor.equals("x")) {
                resultado *= valor;
            }
            if(ultimoValor.equals("=")){
                resultado = valor;
            }
            if(!hasError){
                pantalla.setText("" + resultado);
            }else {
                pantalla.setText("No se puede dividir por cero");
            }

        }
    }
}
