package TrabajoIndividialProcesadorDeTexto;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.StyledDocument;
import javax.swing.text.StyledEditorKit;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.io.*;

public class Lamina extends JPanel implements ActionListener {

    private JTextPane areaDeTexto;
    private JMenu archivo;
    private JMenu fuente;
    private JMenu estilo;
    private JMenu tamanio;

    public Lamina() {

        setLayout(new BorderLayout());

        JPanel laminaParaMenu = new JPanel();

        JMenuBar barraMenu = new JMenuBar();

        archivo = new JMenu("Archivo");
        fuente = new JMenu("Fuente");
        estilo = new JMenu("Estilo");
        tamanio = new JMenu("Tamaño");

        barraMenu.add(archivo);
        barraMenu.add(fuente);
        barraMenu.add(estilo);
        barraMenu.add(tamanio);

        //-----------------------------------------------------

        JMenuItem abrir = new JMenuItem("Abrir");
        JMenuItem guardar = new JMenuItem("Guardar");

        abrir.addActionListener(this);
        guardar.addActionListener(this);

        archivo.add(abrir);
        archivo.add(guardar);

        //------------------------------------------------------

        configuracionDeMenu("Arial", "Fuente", "Arial", 0);
        configuracionDeMenu("Courier New", "Fuente", "Courier New", 0);
        configuracionDeMenu("Verdana", "Fuente", "Verdana", 0);

        //--------------------------------------------------

        configuracionDeMenu("Negrita", "Estilo", "", Font.BOLD);
        configuracionDeMenu("Cursiva", "Estilo", "", Font.ITALIC);

        //--------------------------------------------------

        ButtonGroup tamagno_letra = new ButtonGroup();

        for (int i = 0; i <= 36; i++) {

            JRadioButtonMenuItem tam = new JRadioButtonMenuItem("" + i);
            tamagno_letra.add(tam);
            tam.addActionListener(new StyledEditorKit.FontSizeAction("Cambia Tamaño", i));
            tamanio.add(tam);
            i++;
        }

        //-----------------------------------------------------

        laminaParaMenu.add(barraMenu);

        add(laminaParaMenu, BorderLayout.NORTH);

        areaDeTexto = new JTextPane();

        JScrollPane scroll = new JScrollPane(areaDeTexto);

        add(scroll);

    }

    private void configuracionDeMenu(String texto, String menu, String tipoDeLetra, int estilos) {

        JMenuItem elemento = new JMenuItem(texto);

        if (menu == "Fuente") {

            fuente.add(elemento);

            if (tipoDeLetra == "Arial") {
                elemento.addActionListener(new StyledEditorKit.FontFamilyAction("Cambia tipo de letra", "Arial"));

            } else if (tipoDeLetra == "Courier New") {
                elemento.addActionListener(new StyledEditorKit.FontFamilyAction("Cambia tipo de letra", "Courier New"));

            } else if (tipoDeLetra == "Verdana") {
                elemento.addActionListener(new StyledEditorKit.FontFamilyAction("Cambia tipo de letra", "Verdana"));
            }

        } else if (menu == "Estilo") {

            estilo.add(elemento);

            if (estilos == Font.BOLD) {
                elemento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
                elemento.addActionListener(new StyledEditorKit.BoldAction());

            } else if (estilos == Font.ITALIC) {
                elemento.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K, InputEvent.CTRL_DOWN_MASK));
                elemento.addActionListener(new StyledEditorKit.ItalicAction());

            }
        }

    }

    public void guardadoTemporal(File archivoTemporal) {

        StyledDocument doc = (StyledDocument)areaDeTexto.getDocument();

        HTMLEditorKit kit = new HTMLEditorKit();

        BufferedOutputStream out;

        try {
            FileOutputStream df = new FileOutputStream(archivoTemporal.getAbsolutePath());

            out = new BufferedOutputStream(df);

            kit.write(out, doc, doc.getStartPosition().getOffset(), doc.getLength());

        } catch (FileNotFoundException ev) {
            JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());

        } catch (IOException ev){
            JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());

        } catch (BadLocationException ev){
            JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());
        }
    }

    public void recuperacionDeArchivoSinGuardar(File archivoTemporal) {

        try {
            areaDeTexto.setPage(archivoTemporal.toURI().toURL());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String s = e.getActionCommand();

        if (s.equals("Abrir")) {

            JFileChooser j = new JFileChooser();

            //Invoque la función showsOpenDialog para mostrar el diálogo de abrir
            int r = j.showOpenDialog(null);

            // Si el usuario selecciona un archivo
            if (r == JFileChooser.APPROVE_OPTION) {

                try {
                    File archivo = new File(j.getSelectedFile().getAbsolutePath());

                    areaDeTexto.setPage(archivo.toURI().toURL());

                }
                catch (FileNotFoundException ev){
                    JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());

                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }

            } else {
                JOptionPane.showMessageDialog(null, "Canceló la operación");
            }

        } else if (s.equals("Guardar")) {

            JFileChooser selector = new JFileChooser();
            selector.setMultiSelectionEnabled(false);

            int option = selector.showSaveDialog(null);

            if (option == JFileChooser.APPROVE_OPTION) {

                StyledDocument doc = (StyledDocument)areaDeTexto.getDocument();

                HTMLEditorKit kit = new HTMLEditorKit();

                BufferedOutputStream out;

                try {
                    out = new BufferedOutputStream(new FileOutputStream(selector.getSelectedFile().getAbsoluteFile()));

                    kit.write(out, doc, doc.getStartPosition().getOffset(), doc.getLength());

                } catch (FileNotFoundException ev) {
                    JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());

                } catch (IOException ev){
                    JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());

                } catch (BadLocationException ev){
                    JOptionPane.showMessageDialog(null, ev.getLocalizedMessage());
                }

            } else {
                JOptionPane.showMessageDialog(null, "Canceló la operación");
            }
        }
    }
}