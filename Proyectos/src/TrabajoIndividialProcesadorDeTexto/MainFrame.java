package TrabajoIndividialProcesadorDeTexto;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class MainFrame extends JFrame {

    private Lamina lamina;
    private File archivotemporal;

    public MainFrame(){

        setTitle("<<<<Procesador de Texto>>>>");
        setBounds(500,200,400,400);

        lamina = new Lamina();
        add(lamina);

        setVisible(true);

        archivotemporal = new File("Buffer");

        addWindowListener(new EventoDeVentana());
    }

    private class EventoDeVentana extends WindowAdapter {

        @Override
        public void windowOpened(WindowEvent e) {

            if(archivotemporal.exists()){
                lamina.recuperacionDeArchivoSinGuardar(archivotemporal);
            }
        }

        @Override
        public void windowClosing(WindowEvent e) {

            lamina.guardadoTemporal(archivotemporal);
        }
    }
}